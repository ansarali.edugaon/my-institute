package com.example.edugaon.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.edugaon.R
import com.example.edugaon.adapters.PracticeAdapter
import com.example.edugaon.models.Practices
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),PracticeAdapter.OnPracticeItemListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initPracticeRecyclerView()
    }

    private fun initPracticeRecyclerView() {
        val adapter = PracticeAdapter(
            listOf(
                Practices(
                    1,
                    "Upi Payment",
                    "Apply upi payment method for money payment.",
                    R.drawable.icon_upi
                )
            ),this
        )
        recycler_practice.adapter = adapter
        recycler_practice.layoutManager = LinearLayoutManager(this)
    }

    override fun onClickListener(data: Practices) {
        when(data.title){
            "Upi Payment" ->{
                val intent = Intent(this,UpiPaymentActivity::class.java)
                startActivity(intent)
            }
        }
    }
}



