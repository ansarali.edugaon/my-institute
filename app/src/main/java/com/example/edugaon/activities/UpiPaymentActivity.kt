package com.example.edugaon.activities

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.edugaon.R
import com.shreyaspatil.EasyUpiPayment.EasyUpiPayment
import com.shreyaspatil.EasyUpiPayment.listener.PaymentStatusListener
import com.shreyaspatil.EasyUpiPayment.model.PaymentApp
import com.shreyaspatil.EasyUpiPayment.model.TransactionDetails
import kotlinx.android.synthetic.main.activity_upi_payment.*
import java.sql.Timestamp
import java.util.*
import android.net.NetworkInfo

import android.net.ConnectivityManager

import android.content.Intent
import android.net.Uri
import android.util.Log

import androidx.core.app.ActivityCompat.startActivityForResult



class UpiPaymentActivity : AppCompatActivity(){
    private val UPI_PAYMENT = 100
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upi_payment)
        mtBtn_submit_upi.setOnClickListener {
            validateEditFields()
        }
    }

    private fun validateEditFields(){
        when{
            edit_amount_upi.text!!.isEmpty() ->{
                edit_amount_upi.error = "Please enter your amount here!"
                edit_amount_upi.requestFocus()
            }
            edit_upiNo_upi.text!!.isEmpty() ->{
                edit_upiNo_upi.error = "Please enter your upi number!"
                edit_name_upi.requestFocus()
            }
            edit_name_upi.text!!.isEmpty() ->{
                edit_name_upi.error = "Please enter upi name here!"
                edit_name_upi.requestFocus()
            }else ->{
                //makePayment()

                payUsingUpi()
            }
        }
    }


    private fun payUsingUpi() {

        val amount = edit_amount_upi.text.toString()
        val upiNo = edit_upiNo_upi.text.toString()
        val name = edit_name_upi.text.toString()
        val desc = edit_desc_upi.text.toString()

        val uri: Uri = Uri.parse("upi://pay").buildUpon()
            .appendQueryParameter("pa", upiNo)
            .appendQueryParameter("pn", name)
            .appendQueryParameter("tn", desc)
            .appendQueryParameter("am", amount)
            .appendQueryParameter("cu", "INR")
            .build()
        val upiPayIntent = Intent(Intent.ACTION_VIEW)
        upiPayIntent.data = uri

        val chooser = Intent.createChooser(upiPayIntent, "Pay with")

        if (null != chooser.resolveActivity(packageManager)) {
            startActivityForResult(chooser, UPI_PAYMENT)
        } else {
            Toast.makeText(
                this,
                "No UPI app found, please install one to continue",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            UPI_PAYMENT -> if (RESULT_OK == resultCode || resultCode == 11) {
                if (data != null) {
                    val trxt = data.getStringExtra("response")
                    Log.d("UPI", "onActivityResult: $trxt")
                    val dataList: ArrayList<String?> = ArrayList()
                    dataList.add(trxt)
                    upiPaymentDataOperation(dataList)
                } else {
                    Log.d("UPI", "onActivityResult: " + "Return data is null")
                    val dataList: ArrayList<String?> = ArrayList()
                    dataList.add("nothing")
                    upiPaymentDataOperation(dataList)
                }
            } else {
                Log.d(
                    "UPI",
                    "onActivityResult: " + "Return data is null"
                ) //when user simply back without payment
                val dataList: ArrayList<String?> = ArrayList()
                dataList.add("nothing")
                upiPaymentDataOperation(dataList)
            }
        }
    }

    private fun upiPaymentDataOperation(data: ArrayList<String?>) {
        if (isConnectionAvailable(this)) {
            var str = data[0]
           // Log.d("UPIPAY", "upiPaymentDataOperation: $str")
            var paymentCancel = ""
            if (str == null) str = "discard"
            var status = ""
            var approvalRefNo = ""
            val response = str.split("&").toTypedArray()
            for (i in response.indices) {
                val equalStr = response[i].split("=").toTypedArray()
                if (equalStr.size >= 2) {
                    if (equalStr[0].equals("Status", ignoreCase = true)) {
                        status = equalStr[1].lowercase(Locale.getDefault())
                    } else if (equalStr[0].lowercase(Locale.getDefault()) == "ApprovalRefNo".lowercase(
                            Locale.getDefault()
                        ) || equalStr[0].lowercase(Locale.getDefault()) == "txnRef".lowercase(Locale.getDefault())
                    ) {
                        approvalRefNo = equalStr[1]
                    }
                } else {
                    paymentCancel = "Payment cancelled by user."
                }
            }
            if (status == "success") {
                //Code to handle successful transaction here.
                Toast.makeText(this, "Transaction successful.", Toast.LENGTH_SHORT)
                    .show()

            } else if ("Payment cancelled by user." == paymentCancel) {
                Toast.makeText(this, "Payment cancelled by user.", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(
                    this,
                    "Transaction failed.Please try again",
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            Toast.makeText(
                this,
                "Internet connection is not available. Please check and try again",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun isConnectionAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val netInfo = connectivityManager.activeNetworkInfo
            if (netInfo != null && netInfo.isConnected
                && netInfo.isConnectedOrConnecting
                && netInfo.isAvailable
            ) {
                return true
            }
        }
        return false
    }


//    private fun makePayment() {
//        val traId = Date().time
//        val amount = edit_amount_upi.text.toString()
//        val upiNo = edit_upiNo_upi.text.toString()
//        val name = edit_name_upi.text.toString()
//        val desc = edit_desc_upi.text.toString()
//
//        val easyPayment = EasyUpiPayment.Builder()
//            .with(this)
//            .setAmount(amount)
//            .setPayeeVpa(upiNo)
//            .setPayeeName(name)
//            .setDescription(desc)
//            .setTransactionId(traId.toString())
//            .setTransactionRefId(traId.toString())
//            .build()
//
//        //easyPayment.setDefaultPaymentApp(PaymentApp.BHIM_UPI)
//        easyPayment.startPayment()
//        easyPayment.setPaymentStatusListener(this)
//    }
//
//    override fun onTransactionCompleted(transactionDetails: TransactionDetails?) {
//        val traDetails = "${transactionDetails?.status.toString()} Transaction ID : ${transactionDetails?.transactionId}\n"
//        txt_traDetails_upi.visibility = View.VISIBLE
//        txt_traDetails_upi.text = traDetails
//    }
//
//    override fun onTransactionSuccess() {
//        Toast.makeText(this,"Your payment successfully done",Toast.LENGTH_SHORT).show()
//    }
//
//    override fun onTransactionSubmitted() {
//
//    }
//
//    override fun onTransactionFailed() {
//        Toast.makeText(this,"Your payment failed",Toast.LENGTH_SHORT).show()
//    }
//
//    override fun onTransactionCancelled() {
//        Toast.makeText(this,"Your payment cancelled",Toast.LENGTH_SHORT).show()
//    }
//
//    override fun onAppNotFound() {
//        Toast.makeText(this,"There is no upi app available",Toast.LENGTH_SHORT).show()
//    }


}