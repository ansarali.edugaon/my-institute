package com.example.edugaon.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.edugaon.R
import com.example.edugaon.models.Practices
import kotlinx.android.synthetic.main.lyt_practice_item.view.*

class PracticeAdapter(private val practicesList:List<Practices>, val onPracticeItemListener: OnPracticeItemListener):RecyclerView.Adapter<PracticeAdapter.PracticeViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PracticeViewHolder {
        return PracticeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.lyt_practice_item,parent,false),onPracticeItemListener)
    }

    override fun onBindViewHolder(holder: PracticeViewHolder, position: Int) {
        holder.bind(practicesList[position])
        holder.practiceData = practicesList[position]
    }

    override fun getItemCount(): Int {
        return practicesList.size
    }

    class PracticeViewHolder(itemView: View,onPracticeItemListener: OnPracticeItemListener):RecyclerView.ViewHolder(itemView){
        lateinit var practiceData:Practices
        fun bind(data:Practices){
           // Glide.with(itemView.context).load(data.image).into(itemView.img_topic_practiceItem)
            itemView.img_topic_practiceItem.setImageResource(data.image!!)
            itemView.txt_title_practiceItem.text = data.title
            itemView.txt_desc_practiceItem.text = data.des

        }
        init {
            itemView.setOnClickListener {
                onPracticeItemListener.onClickListener(practiceData)
            }
        }
    }

    interface OnPracticeItemListener{
        fun onClickListener(data:Practices)
    }
}