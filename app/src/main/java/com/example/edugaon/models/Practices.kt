package com.example.edugaon.models

data class Practices(
    val id:Int? = null,
    val title:String? = null,
    val des:String? = null,
    val image:Int? = null
)